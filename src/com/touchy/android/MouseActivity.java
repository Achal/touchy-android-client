package com.touchy.android;

import android.os.Bundle;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MouseActivity extends Activity {
	TextView tv1,tv2,tv3,tv4,tv5,tv6;
	int pointer_down_at_time = 0;
	int pointer_up_at_time = 0;
	class mEvent{
		public float x, y,dx,dy;
		public long time;
	}
	mEvent up,down,move,current;
	UDPSocket socket;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tv1 = (TextView) findViewById(R.id.textView1);
        tv2 = (TextView) findViewById(R.id.textView2);
        tv3 = (TextView) findViewById(R.id.textView3);
        tv4 = (TextView) findViewById(R.id.textView4);
        tv5 = (TextView) findViewById(R.id.textView5);
        tv6 = (TextView) findViewById(R.id.textView6);
        up = new mEvent();
        down = new mEvent();
        move = new mEvent();
        socket = new UDPSocket();
        tv6.setText("dy : " + socket.init("192.168.137.1", 54321));
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl1);
        rl.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				if(arg1.getPointerCount()==1){
					int action = arg1.getAction();
					double distance, timediff;
					switch(action)
					{
						case MotionEvent.ACTION_DOWN:
							down.x = arg1.getX();
							down.y = arg1.getY();
							down.time =  arg1.getEventTime();
							current = down;
							break;
						case MotionEvent.ACTION_UP:
							up.x = arg1.getX();
							up.y = arg1.getY();
							up.time = arg1.getEventTime();
							current = up;
							up.dx = up.x - down.x;
							up.dy = up.y - down.y;
							distance = Math.sqrt(up.dx*up.dx + up.dy*up.dy);
							timediff = up.time - down.time;
							if(distance<10 && timediff<100)sendToServer("mouse","click",down.x,down.y,-1,-1);
							else if(distance<10)sendToServer("mouse","right_click",down.x,down.y,-1,-1);
							break;
						case MotionEvent.ACTION_MOVE:
							move.x = arg1.getX();
							move.y = arg1.getY();
							move.time = arg1.getEventTime();
							move.dx = move.x - down.x;
							move.dy = move.y - down.y;
							current = move;
							distance = Math.sqrt(move.dx*move.dx + move.dy*move.dy);
							timediff = move.time - down.time;
							if(distance>10 || timediff>100)sendToServer("mouse","move",move.x,move.y,move.dx,move.dy);
							break;
					}
					
				}
				else{
					if(arg1.getAction()==MotionEvent.ACTION_POINTER_2_UP){
						sendToServer("mouse","toggle_darg_drop",-1,-1,-1,-1);
					}
				}
				return true;
			}
		});
    }
    String encrypt(String action, String event, float x, float y, float dx, float dy){
    	String str = "#action:"+ action
    			+"#event:" + event
    			+ "#x:"+Float.toString(x)
    			+ "#y:"+Float.toString(y)
    			+ "#dx:"+Float.toString(dx)
    			+ "#dy:"+Float.toString(dy);
    	return str;
    }
    boolean sendToServer(String action,String event, float x, float y, float dx, float dy){
    	tv1.setText("Action : " + action);
    	tv2.setText("event : " + event);
    	tv3.setText("x : " + Float.toString(x) +"y : " + Float.toString(y));
    	tv4.setText("dx : " + Float.toString(dx) +"dy : " + Float.toString(dy));
    	if(event == "toggle_darg_drop") tv5.setText(event);
    	else if(event == "click") tv5.setText(" ");
    	String str = encrypt(action,event,x,y,dx,dy);
    	tv6.setText("dx : " + socket.send(str));
		return true;
    }
   
}