package com.touchy.android;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

public class MainActivity extends Activity{
	
	Button connect,exit;
	EditText ip;
	RadioGroup rg;
	LinearLayout iplayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_layout);
		connect = (Button) findViewById(R.id.connect);
		exit = (Button) findViewById(R.id.exit);
		ip = (EditText) findViewById(R.id.ip);
		rg = (RadioGroup) findViewById(R.id.rg);
		iplayout = (LinearLayout) findViewById(R.id.iplayout);
		WifiManager wi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = null;
		if (wi.isWifiEnabled()==true) {
			
			if (wi.reconnect()){
				info = wi.getConnectionInfo();
				Log.d("WIFI","bssid "+info.getBSSID());
				Log.d("WIFI","mac "+info.getMacAddress());
				Log.d("WIFI","ssid "+info.getSSID());
				Log.d("WIFI","toString "+info.toString());
				Log.d("WIFI","ip "+String.valueOf(info.getIpAddress()));
				Log.d("WIFI","linkspeed "+String.valueOf(info.getLinkSpeed()));
				Log.d("WIFI","Netrorkid "+String.valueOf(info.getNetworkId()));
			}
			else Log.d("WIFI","not connected");
		}
		else Log.d("WIFI","not enabled");
	}
}
